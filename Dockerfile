FROM archlinux/archlinux:base-devel
LABEL org.opencontainers.image.description="A work-in-progress, easy to use, set up and configure Arch Linux derivative"

RUN pacman -Syu git --noconfirm

RUN useradd -u 1000 temp-user

USER temp-user
RUN git clone https://gitlab.com/crystal-linux/pkgbuilds/crystal-keyring.git /tmp/crystal-keyring && cd /tmp/crystal-keyring && makepkg --noconfirm -s
RUN git clone https://gitlab.com/crystal-linux/pkgbuilds/crystal-mirrorlist.git /tmp/crystal-mirrorlist && cd /tmp/crystal-mirrorlist && makepkg --noconfirm -s

USER root
RUN userdel temp-user

RUN pacman --noconfirm -U /tmp/crystal-{keyring,mirrorlist}/*.zst && rm -rfv /tmp/crystal-{keyring,mirrorlist}
RUN pacman-key --init && pacman-key --populate crystal
RUN curl https://gitlab.com/crystal-linux/misc/iso/-/raw/main/crystal/pacman.conf -o /etc/pacman.conf && sed -i 's/^CheckSpace/#CheckSpace/g' /etc/pacman.conf

RUN pacman -Syu crystal-core crystal-branding --noconfirm

RUN yes | pacman -Scc
