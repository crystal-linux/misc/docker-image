# Crystal Docker Image

The official Crystal Linux Docker image


## 🫂 Support

**Support** is available in our [Discord](https://getcryst.al/discord) and the [Matrix](https://matrix.to/#/#space:getcryst.al). If you face any issues with the software, feel free to open an issue on this repository.

## 💾 Installation
**To use this image with distrobox/toolbox**

Distrobox:
```bash 
 $ distrobox create -n insertnamehere -i registry.gitlab.com/crystal-linux/misc/docker:latest
```

Toolbox:
```bash
 $ git clone https://gitlab.com/crystal-linux/misc/docker.git && buildah bud -f docker/Containerfile-toolbox -t crystal-toolbox && toolbox create -i crystal-toolbox
 ```
 
**Pull this image with Docker**

```bash
 $ docker pull registry.gitlab.com/crystal-linux/misc/docker:latest
```

**Pull this image with Podman**

```bash
 $ podman pull registry.gitlab.com/crystal-linux/misc/docker:latest
```
## 🙌 Contributing

If you'd like to contribute to **Crystal Docker Image**, please follow the [Crystal Linux contributing guidelines](https://gitlab.com/crystal-linux/info/-/blob/main/CONTRIBUTING.md)!

We are also constantly looking for translators for our i18n-enabled projects! If you speak more than one language, consider helping out on our [Weblate](https://i18n.getcryst.al)!

![https://i18n.getcryst.al/engage/crystal-linux/](https://i18n.getcryst.al/widgets/crystal-linux/-/287x66-black.png)


## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)

![](https://gitlab.com/crystal-linux/misc/branding/-/raw/main/banners/README-banner.png)
